<?php


class GF_Field_Initials extends GF_Field {
	
	public $type = 'initials';
	
	public function get_form_editor_button() {
		return array(
			'group' => 'advanced_fields',
			'text'  => $this->get_form_editor_field_title(),
		);
	}
	
	public function get_form_editor_field_title() {
		return esc_attr__( 'Initials', 'gravityforms' );
	}
	
	public function get_form_editor_field_settings() {
		return array(
			'conditional_logic_field_setting',
			'error_message_setting',
			'description_setting',
			'label_setting',
			'rules_setting',
			'css_class_setting',
		);
	}
	
	public function is_conditional_logic_supported() {
		return true;
	}
	
	public function get_field_input( $form, $value = '', $entry = null ) {
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();
		
		$form_id  = $form['id'];
		$id       = intval( $this->id );
		$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
		
		$disabled_text = $is_form_editor ? "disabled='disabled'" : '';
		$class_suffix  = $is_entry_detail ? '_admin' : '';
		
		$first_tabindex = GFCommon::get_tabindex();
		
		$first_markup = "<span id='{$field_id}_1_container' class='initials'>
                                                    <input type='text' name='input_{$id}' id='{$field_id}_1' maxlength='2' minlength='2' value='{$value}' {$first_tabindex} {$disabled_text} />
                                                </span>";
		
		return "<div class='{$class_suffix} ginput_container gfield_trigger_change' id='{$field_id}'>
                    {$first_markup}
                    <div class='gf_clear gf_clear_complex'></div>
        		</div>";
	}
	
	public function get_field_content( $value, $force_frontend_label, $form ) {
		
		$form_id = (int) rgar( $form, 'id' );
		
		$field_label = $this->get_field_label( $force_frontend_label, $value );
		if ( ! in_array( $this->inputType, array( 'calculation', 'singleproduct' ), true ) ) {
			// Calculation field put a screen reader text in the label so do not escape it.
			$field_label = esc_html( $field_label );
		}
		
		$validation_message_id = 'validation_message_' . $form_id . '_' . $this->id;
		$validation_message    = ( $this->failed_validation && ! empty( $this->validation_message ) ) ? sprintf( "<div id='%s' class='gfield_description validation_message gfield_validation_message'>%s</div>", $validation_message_id, $this->validation_message ) : '';
		
		$is_form_editor  = $this->is_form_editor();
		$is_entry_detail = $this->is_entry_detail();
		$is_admin        = $is_form_editor || $is_entry_detail;
		
		$required_div = $this->isRequired ? '<span class="gfield_required">' . $this->get_required_indicator() . '</span>' : '';
		
		$admin_buttons = $this->get_admin_buttons();
		
		$target_input_id = $this->get_first_input_id( $form );
		
		$label_tag = $this->get_field_label_tag( $form );
		
		$for_attribute = empty( $target_input_id ) || $label_tag === 'legend' ? '' : "for='{$target_input_id}'";
		
		$admin_hidden_markup = ( $this->visibility == 'hidden' ) ? $this->get_hidden_admin_markup() : '';
		
		$description = $this->get_description( $this->description, 'gfield_description' );
		
		$field_content = sprintf( "%s%s<$label_tag class='%s' $for_attribute >%s%s</$label_tag> <div class='initials_field_container'> {FIELD}%s </div> %s", $admin_buttons, $admin_hidden_markup, esc_attr( $this->get_field_label_class() ), $field_label, $required_div, $description, $validation_message );
		
		
		return $field_content;
		
	}
	
	public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {
		
		return $value;
	}
	
	
}

