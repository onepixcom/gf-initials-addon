<?php

/*
Plugin Name: Initial Add-One
*/

define( 'GF_SIMPLE_ADDON_VERSION', '2.0' );

if ( class_exists( 'GF_Field' ) ) {
	
	add_action( 'gform_loaded', [ 'GF_Initials_AddOn_Bootstrap', 'load' ], 5 );
	add_filter( 'gform_print_styles', [ 'GF_Initials_AddOn_Bootstrap', 'add_styles' ], 10, 2 );
	add_filter( 'gform_preview_styles', [ 'GF_Initials_AddOn_Bootstrap', 'add_styles' ], 10, 2 );
	add_action( 'admin_head', [ 'GF_Initials_AddOn_Bootstrap', 'enqueue_form_editor_style' ] );
	
	
	class GF_Initials_AddOn_Bootstrap {
		
		public static function load() {
			
			require_once( 'class-gf-field-initials.php' );
			
			GF_Fields::register( new GF_Field_Initials() );
		}
		
		public static function add_styles() {
			
			wp_register_style( 'print_initials_styles', plugins_url( '/initials-styles.css', __FILE__ ) );
			
			return array( 'print_initials_styles' );
		}
		
		public static function enqueue_form_editor_style() {
			
			if ( RGForms::is_gravity_page() ) {
				
				wp_enqueue_style( 'print_initials_styles', plugins_url( '/initials-styles.css', __FILE__ ) );
				
			}
		}
		
		
	}
	
}



